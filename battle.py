from player import Player
from monster import Monster

'''
    This class describes a battle
    A battle has a player and a monster
'''
class Battle(object):
    
    def __init__(self, player: Player, monster: Monster) -> None:
        
        self.player: Player = player
        self.monster: Monster = monster
        self.turn = 0
        
        
    '''
        The player and the monster go back and fourth hurting each other till one dies
    '''
    def battle(self):
        
        while self.player.health > 0 and self.monster.health > 0:
            print(f"Player health: {self.player.health}")
            print(f"Monster health: {self.monster.health}")
            if self.turn == 0:
                if self.player.attack():
                    self.monster.health -= self.player.attackDamage # Decreases the monsters health
                
            else:
                if self.monster.attack():
                    self.player.health -= self.monster.attackDamage # Decreases the players health
                    
            
                    
            self.turn = not self.turn # Switches the turns

        print(f"Player health: {self.player.health}")
        print(f"Monster health: {self.monster.health}") 
        if self.player.health <= 0:
            return False
        
        else:
            return True