from turtle import *
from functions import *
from player import Player
from monster import Monster
from battle import Battle
from room import Room
import time
import json
import sys

hideturtle()
speed(0)

'''
    Tells the story line for the final boss room
'''  
def finalBossRoom(room: Room, player: Player, window, deaths):
    
    narrTurtle = writeNarration(f'HAHHAHAH time for you to face me {player.name}. You have died {deaths} times')
    time.sleep(2)
    narrTurtle.clear()

    check = False


    for key, val in player.conditions.items():
       if val is False:
           check = True

    if "medicine" in player.inventory and player.health < 100:
        narrTurtle.clear()
        narrTurtle = writeNarration("Do you want to use the medicine in your inventory to bring your health to 100")
        time.sleep(2)
        narrTurtle.clear()

        ans = textinput(room.name ,room.prompts["medicine"])

        if ans == "1":
            player.health = 100
            player.inventory.remove("medicine")
            drawStartHealth()

    if check == False and player.health == 100:
        player.attackDamage = 1000
        battle = Battle(player, Monster("Final Boss",99,1000))
        battle.battle()

        if player.health <= 0:
            decreaseHealthBar(player.health)
            narrTurtle.clear()
            narrTurtle = writeNarration('You Lose. HAHAHHA you will never leave here')
            time.sleep(2)
            narrTurtle.clear()
            player.health = 100
            return "lose"

        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('NOOOOOO you beat me. I will allow you to leave now')
            time.sleep(2)
            narrTurtle.clear()
            return "win"
    else:
        narrTurtle.clear()
        narrTurtle = writeNarration('You have not spent enough time here to defeat the boss. You die')
        time.sleep(2)
        narrTurtle.clear()
        
        return "died"

'''
    Tells the story line for the start room
'''  
def startRoom(room: Room, player: Player, window):

    while True:
            room1Prompt1NarrText = f''' 
            Ah, it would appear our friend {player.name} is finally awake. 
            His bar crawl seems to have taken a turn, and he does
            not seem to remember much. What time is it, even?
            I guess what {player.name} would like to do first is to find
            a clock. He would go through the door to pursue this goal
            '''
            narrTurtle = writeNarration(room1Prompt1NarrText.strip())
        
            firstRoom = textinput(room.name ,room.prompts["firstAction"])
            
            if firstRoom == "1":
                narrTurtle.clear()
                room.goToNewRoom("assets/fountain.gif", window)
                return
                
                
            elif firstRoom == '2':
                
                room1Promt2NarrText = f''' 
                {player.name} was always a curious one. He looked around the 
                room and found that it was made out of solid rock as it was 
                a dungeon or cave of sorts. Knowing that there was nothing
                in the room for him, he promptly left through the door.
                '''
                
                narrTurtle.clear()
                narrTurtle = writeNarration(room1Promt2NarrText)
                time.sleep(10)
                
                narrTurtle.clear()
                room.goToNewRoom("assets/fountain.gif", window)
                return
                
            else:
                narrTurtle.clear()
                narrTurtle = writeNarration('Invalid input please try again')
                time.sleep(2)
                narrTurtle.clear()
'''
    Tells the story line for the fountain room
'''  
def fountainRoom(room: Room, player: Player, window):

    while True:
        key = True
        hand = True

        narrTurtle = writeNarration("You see a mysterious fountain")

        secondRoom = textinput(room.name, room.prompts['firstAction'])
        
        if secondRoom == "1": # Investigate Fountain
            narrTurtle.clear()
            while True and key and hand:
                founatin = textinput(room.name, room.prompts['investigateFountain'])
                
                if founatin == "1": # Drinks the water
                    narrTurtle.clear()
                    narrTurtle = writeNarration('Drinking the water brought you the Big Boss Battle')
                    time.sleep(2)
                    narrTurtle.clear()

                    room.goToNewRoom('assets/finalboss.gif', window)
                    narrTurtle.clear()
                    return "finalBossRoom"

                
                elif founatin == "2": # Puts hand in water
                    while True :
                        secondAction = textinput(room.name, room.prompts['secondAction'])
                        if secondAction == "1":
                            if "key" not in player.inventory:
                                narrTurtle.clear()
                                narrTurtle = writeNarration('You lose your hand and 50 HP. Your accuracy is decreased by 2. Your reward for your sacrafic is a key')

                                player.accuracy -= 2
                                player.health -= 50
                                player.inventory.append("key")
                                decreaseHealthBar(player.health)
                                time.sleep(2)
                                narrTurtle.clear()
                            elif "key" in player.inventory and player.health - 25 > 0:
                                narrTurtle.clear()
                                narrTurtle = writeNarration('You lose your other hand and 25 HP. Your accuracy is decreased by 2')
                                player.health -= 25
                                player.accuracy -= 2
                                decreaseHealthBar(player.health)
                                time.sleep(2)
                                narrTurtle.clear()
                            
                            elif player.health - 25 <= 0:
                                narrTurtle.clear()
                                narrTurtle = writeNarration('You lose')
                                player.health -= 25
                                decreaseHealthBar(player.health)
                                time.sleep(2)
                                narrTurtle.clear()
                                return "died"
                            hand = False
                            break
                        elif secondAction == "2":
                            if player.health - 25 <= 0:
                                narrTurtle.clear()
                                narrTurtle = writeNarration('You died')
                                time.sleep(2)
                                narrTurtle.clear()

                                player.health -= 25
                                decreaseHealthBar(player.health)

                                return "died"
                            else:
                                narrTurtle.clear()
                                narrTurtle = writeNarration('You lose 25 HP')
                                time.sleep(2)
                                narrTurtle.clear()

                                player.health -= 25
                                decreaseHealthBar(player.health)
                                break
                        elif secondAction == "3":
                            narrTurtle.clear()
                            narrTurtle = writeNarration('Your face melts off. You Died')
                            time.sleep(2)
                            narrTurtle.clear()

                            player.health -= 100
                            decreaseHealthBar(player.health)
                            drawStartHealth()
                            return "died"
                            
                        else:
                            narrTurtle.clear()
                            narrTurtle = writeNarration('Invalid input please try again')
                            time.sleep(2)
                            narrTurtle.clear()

                elif founatin == "3": # Goes back to the room
                    if "branch" in player.inventory:
                        narrTurtle.clear()
                        narrTurtle = writeNarration('You reached with the tree branch you found the key! Key is added to your inventory')
                        time.sleep(2)
                        narrTurtle.clear()
                        player.inventory.append("key")
                        key = False
                        break

                else:
                    narrTurtle.clear()
                    narrTurtle = writeNarration('Invalid input please try again')
                    time.sleep(2)
                    narrTurtle.clear()
        
        elif secondRoom == "2": # Investigate Room
            if "branch" in player.inventory:
                narrTurtle.clear()
                narrTurtle = writeNarration('The branch is already in your inventory')
                time.sleep(2)
                narrTurtle.clear()
            else:
                narrTurtle.clear()
                narrTurtle = writeNarration('You found a tree branch. It is now in your inventory')
                player.inventory.append("branch")
                time.sleep(2)
                narrTurtle.clear()
        
        elif secondRoom == "3": # Try to open door
            if "key" in player.inventory:
                narrTurtle.clear()
                narrTurtle = writeNarration("The key in your inventory unlocks the door!")
                time.sleep(2)
                narrTurtle.clear()
                room.goToNewRoom("assets/twodoors.gif", window)
                return "continue"
                
            else:
                narrTurtle.clear()
                narrTurtle = writeNarration("The door is locked")
                time.sleep(2)
                narrTurtle.clear()
        
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('Invalid input please try again')
            time.sleep(2)
            narrTurtle.clear()

'''
    Tells the story line for the 2 door room
'''  
def twoDoorRoom(room: Room, player: Player, window):
    while True:    
      
        narrTurtle = writeNarration("I wonder what these 2 doors lead to")

        leftOrRight = textinput(room.name, room.prompts['leftOrRight'])

        if leftOrRight == "1": #Left
            narrTurtle.clear()
            narrTurtle = writeNarration("You are brought into a room with a goblin that gives you a riddle")
            time.sleep(2)
            narrTurtle.clear()
            room.goToNewRoom("assets/hallgoblin.gif", window)
            return "riddle"

        elif leftOrRight == "2": #Right
            narrTurtle.clear()
            narrTurtle = writeNarration("You go into a room with a long hallway")
            time.sleep(2)
            narrTurtle.clear()

            room.goToNewRoom("assets/hall.gif", window)
            return "longhallway"
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('Invalid input please try again')
            time.sleep(2)
            narrTurtle.clear()

'''
    Tells the story line for the goblin riddle room
'''     
def goblinRiddleRoom(room: Room, player: Player, window):
    
    narrTurtle = writeNarration(f"Hellllo there {player.name}, please answer my riddle to pass")
    time.sleep(2)
    narrTurtle.clear()

    answer = textinput(room.name, room.prompts['riddle'])
    
    if answer == "23": #Correct
        narrTurtle.clear()
        narrTurtle = writeNarration("Correct! You may advance")
        time.sleep(2)
        narrTurtle.clear()
        room.goToNewRoom("assets/threedoors.gif", window)
        return "continue"
    else:
         # Fight Goblin
        narrTurtle.clear()
        narrTurtle = writeNarration("Incorrect! Now I must fight you!")
        time.sleep(2)
        narrTurtle.clear()

        battle = Battle(player,Monster("Riddle Goblin", 5, 20)) #Battles the goblin
        result = battle.battle()

       

        if result:
            narrTurtle.clear()
            narrTurtle = writeNarration('You beat the Riddle Goblin. You may continue')
            time.sleep(2)
            narrTurtle.clear()
            player.goblinDead = True
            decreaseHealthBar(player.health)
            room.goToNewRoom("assets/threedoors.gif", window)
            return "continue"
            
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('You lost to the Riddle Goblin. You Died')
            time.sleep(2)
            narrTurtle.clear()

            player.health -= 100
            decreaseHealthBar(player.health)
            drawStartHealth()
            return "died"
        
'''
    Tells the story line for the three door room
'''
def threeDoorRoom(room: Room, player: Player, window):
    while True:
        narrTurtle = writeNarration("You enter a room with 3 Doors. Select one")
        time.sleep(2)
        narrTurtle.clear()

        answer = textinput(room.name, room.prompts['firstAction'])

        if answer == "1":
            narrTurtle.clear()
            narrTurtle = writeNarration("You enter a room and the floor falls under you. You die")
            time.sleep(2)
            narrTurtle.clear()

            return "died" # User dies, restart game
        elif answer == "2":
            
            while True: # Had to use multiple while loops to add "fake rooms" inside of a room
                narrTurtle.clear()
                narrTurtle = writeNarration("You enter another room with 3 doors. Pick one to enter")
                time.sleep(2)
                narrTurtle.clear()

                answer = textinput(room.name, room.prompts['secondAction'])

                if answer == "1":
                    continue

                elif answer == "2":
                    while True:
                        narrTurtle.clear()
                        narrTurtle = writeNarration("You find a sword with no handle lodged in a stone")
                        time.sleep(2)
                        narrTurtle.clear()

                        answer = textinput(room.name, room.prompts['sword'])

                        if answer == "1":
                            if player.conditions["ancientSword"] is False:
                                if "branch" in player.inventory: # Checks to see if the branch is the player inventory
                                    narrTurtle.clear()
                                    narrTurtle = writeNarration("You have created the ancient sword. This might be powerful enough to beat the final boss")
                                    time.sleep(2)
                                    narrTurtle.clear()
                                    player.conditions["ancientSword"] = True # Need this to beat the game
                                    break
                                else:
                                    narrTurtle.clear()
                                    narrTurtle = writeNarration("You feel the life drain from you and you die")
                                    time.sleep(2)
                                    narrTurtle.clear()

                                    return "died"
                            else:
                                narrTurtle.clear()
                                narrTurtle = writeNarration("You already have the ancient sword in your inventory")
                                time.sleep(2)
                                narrTurtle.clear()
                                break

                        elif answer == "2":
                            narrTurtle.clear()
                            narrTurtle = writeNarration("You return to the room with 3 doors")
                            time.sleep(2)
                            narrTurtle.clear()
                            break

                        else:
                            narrTurtle.clear()
                            narrTurtle = writeNarration('Invalid input please try again')
                            time.sleep(2)
                            narrTurtle.clear()


                elif answer == "3":
                    room.goToNewRoom("assets/hallbiggoblin.gif", window)
                    narrTurtle.clear()
                    narrTurtle = writeNarration("You walk into a room with a big goblin in it. He is scared of you")
                    time.sleep(2)
                    narrTurtle.clear()
                    return "big"
                
                else:
                    narrTurtle.clear()
                    narrTurtle = writeNarration('Invalid input please try again')
                    time.sleep(2)
                    narrTurtle.clear()


        elif answer == "3" and player.door3 is False:
            narrTurtle.clear()
            narrTurtle = writeNarration("You enter in a room with a chest")
            time.sleep(2)
            narrTurtle.clear()

            room.goToNewRoom("assets/hallchest.gif", window)
            return "chest"
        
        elif answer == "3" and player.door3 is True:
            narrTurtle.clear()
            narrTurtle = writeNarration("Door 3 is now locked. Please choose another door")
            time.sleep(2)
            narrTurtle.clear()
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('Invalid input please try again')
            time.sleep(2)
            narrTurtle.clear()

'''
    Tells the story line for the hall chest room
'''
def hallChestRoom(room: Room, player: Player, window):
    while True:
        narrTurtle = writeNarration(f'There is a chest in this room. What should {player.name} do with it?')
        time.sleep(2)
        narrTurtle.clear()

        answer = textinput(room.name, room.prompts['firstAction'])

        if answer == "1":
            while True:
                narrTurtle.clear()
                narrTurtle = writeNarration('A portal opens up from the chest')
                time.sleep(2)
                narrTurtle.clear()
                answer = textinput(room.name, room.prompts['portal'])

                if answer == "1": #Enter portal
                    narrTurtle.clear()
                    narrTurtle = writeNarration('The portal takes you to a human sized bird that will answer any one question you have')
                    time.sleep(2)
                    narrTurtle.clear()
                    room.goToNewRoom("assets/magicalbird.gif", window)
                    return "bird" # Go to bird room


                elif answer == "2": # Leave
                    narrTurtle.clear()
                    narrTurtle = writeNarration('You leave the room and go back to the room with 3 doors')
                    time.sleep(2)
                    narrTurtle.clear()
                    room.goToNewRoom("assets/threedoors.gif", window)
                    return "3doors" # Go to 3 door room

                else:
                    narrTurtle.clear()
                    narrTurtle = writeNarration('Invalid input please try again')
                    time.sleep(2)
                    narrTurtle.clear()
        
        elif answer == "2":
            narrTurtle.clear()
            narrTurtle = writeNarration('You leave the room and go back to the room with 3 doors')
            time.sleep(2)
            narrTurtle.clear()
            room.goToNewRoom("assets/threedoors.gif", window)
            return "continue"
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('Invalid input please try again')
            time.sleep(2)
            narrTurtle.clear()

'''
    Tells the story line for the bird room
'''
def birdRooom(room: Room, player: Player, window):
    while True:
        narrTurtle = writeNarration(f'What question would you like to ask me {player.name}?')
        time.sleep(2)
        narrTurtle.clear()

        answer = textinput(room.name, room.prompts['question'])

        if answer == "1":
            narrTurtle.clear()
            narrTurtle = writeNarration('Do you want to slay the beast to go home?')
            time.sleep(2)
            narrTurtle.clear()

            while True:

                answer = textinput(room.name, room.prompts['fightBoss'])

                if answer == "1":
                    narrTurtle.clear()
                    narrTurtle = writeNarration('You are transported to the room with the final boss')
                    time.sleep(2)
                    narrTurtle.clear()

                    room.goToNewRoom("assets/finalboss.gif", window)
                    return "finalBoss"
                
                elif answer == "2":
                    narrTurtle.clear()
                    narrTurtle = writeNarration('You do not feel ready for the fight so you return to the room with 3 doors')
                    time.sleep(2)
                    narrTurtle.clear()
                    player.door3 = True
                    room.goToNewRoom("assets/threedoors.gif", window)
                    
                    return "continue"

                else:
                    narrTurtle.clear()
                    narrTurtle = writeNarration('Invalid input please try again')
                    time.sleep(2)
                    narrTurtle.clear()

        elif answer == "2":
            narrTurtle.clear()
            narrTurtle = writeNarration('You are in the Lazarus Labyrinth, the realm of revival. If you wish to escape death, you muust find life.')
            time.sleep(3)
            narrTurtle.clear()
            player.door3 = True
            room.goToNewRoom("assets/threedoors.gif", window)
            
            return "continue"

        elif answer == "3":
            narrTurtle.clear()
            narrTurtle = writeNarration('The bird sends mental messages into your brain beyond your comprehension. Your brain melts and you die')
            time.sleep(3)
            narrTurtle.clear()
            player.conditions["understandsLife"] = True # This does not get reset when user dies
            room.goToNewRoom("assets/hall.gif", window)
            return "died"
        
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('Invalid input please try again')
            time.sleep(2)
            narrTurtle.clear()


'''
    The story line for the big goblin room
'''
def bigGoblinRoom(room: Room, player: Player, window):
   

    if player.goblinDead: # Checks to see if the user killed the goblin, does reset as death
       
        narrTurtle = writeNarration("How dare you kill my son! I will only let you pass if you tell me my son's ages")
        time.sleep(2)
        narrTurtle.clear()

        answer = textinput(room.name, room.prompts['old'])

        if answer == "23": # Correct answer
            narrTurtle.clear()
            narrTurtle = writeNarration("The big goblin respects you and lets you leave with a new friend")
            time.sleep(2)
            narrTurtle.clear()
            player.conditions["goblinFriend"] = True
            room.goToNewRoom("assets/threedoors.gif", window)
            return "3doors"
            
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration("Incorrect! Time to fight")
            time.sleep(2)
            narrTurtle.clear()

            battle = Battle(player, Monster("Big Goblin", 30, 100))
            result = battle.battle()

            if result:
                narrTurtle.clear()
                narrTurtle = writeNarration("You defeated the Big Goblin")
                time.sleep(2)
                narrTurtle.clear()
                room.goToNewRoom("assets/threedoors.gif", window)
                return "3doors"
                
            else:
                narrTurtle.clear()
                narrTurtle = writeNarration("You lost to the Big Goblin. You died")
                time.sleep(2)
                narrTurtle.clear()

                return "died"


    else:
        narrTurtle = writeNarration("The big goblin realizes that his son is still alive and has his son kill you. You died")
        time.sleep(2)
        narrTurtle.clear()

        return "died"
    
'''
    The story line for the big goblin room
'''    
def longHallwayRoom(room: Room, player: Player, window):
    while True:
        narrTurtle = writeNarration("What should you do in the long hallway?")
        time.sleep(2)
        narrTurtle.clear()

        answer = textinput(room.name, room.prompts['firstAction'])

        if answer == "1":
            narrTurtle.clear()
            narrTurtle = writeNarration('You continue on and think of some more options you can do')
            time.sleep(2)
            narrTurtle.clear()

            while True:
                answer = textinput(room.name, room.prompts['secondAction'])

                if answer == "1":
                    narrTurtle.clear()
                    narrTurtle = writeNarration('You yell for help, but nothing happens. You feel more powerful though')
                    time.sleep(2)
                    narrTurtle.clear()
                    player.conditions["helpCalled"] = True

                elif answer == "2":
                    narrTurtle.clear()
                    narrTurtle = writeNarration('You are brought into the boss battle room!')
                    time.sleep(2)
                    narrTurtle.clear()
                    room.goToNewRoom("assets/finalboss.gif", window)
                    return "boss"

                elif answer == "3":
                    narrTurtle.clear()
                    narrTurtle = writeNarration('You go back to the room with 2 doors')
                    time.sleep(2)
                    narrTurtle.clear()
                    room.goToNewRoom("assets/twodoors.gif", window)
                    return "2doors"

                else:
                    narrTurtle.clear()
                    narrTurtle = writeNarration('Invalid input please try again')
                    time.sleep(2)
                    narrTurtle.clear() 

        elif answer == "2":
            if "medicine" not in player.inventory: # Checks to see if the medicine item is already in the user inventory
                narrTurtle.clear()
                narrTurtle = writeNarration('You have found medicine that brings your health back to full. It is added to your inventory')
                time.sleep(2)
                narrTurtle.clear()
                player.inventory.append("medicine") # Adds the medicine to players inventory
            else:
                narrTurtle.clear()
                narrTurtle = writeNarration('The medicine is already in your inventory')
                time.sleep(2)
                narrTurtle.clear()
        else:
            narrTurtle.clear()
            narrTurtle = writeNarration('Invalid input please try again')
            time.sleep(2)
            narrTurtle.clear()

def main():
    
    with open('scripts.json', 'r') as openfile: # Loades the premade rooms and their prompts into a dictionary
 
        # Reading from json file
        jsonObj = json.load(openfile)
 
    rooms = {} 
    for room in jsonObj["rooms"]: # Creates a dictionary of Room objects
        rooms[room["name"]] = Room(room["name"], room["path"], room["prompts"])
    

    win = Screen()
    win.setup(width = 1.0, height = 1.0) 
    deaths = 0

    while True:
        play = textinput(rooms["start"].name ,rooms["start"].prompts["play"]).lower()[0] # Asks the user if they want to play
        if play == 'y':
            play = True

            userName = textinput(rooms["start"].name ,rooms["start"].prompts["userName"]) # Asks user for name
            player = Player(userName)

            break
        elif play == 'n':
            play = False
            break
    
    while play:
        restart = True
        player.resetConditions()
        
    
        win.bgpic(rooms["start"].image)
        drawStartHealth()

        startRoom(rooms['start'], player, win)
    
        result = fountainRoom(rooms['fountain'], player, win) 

        if result == "finalBossRoom":
            result = finalBossRoom(rooms['finalBoss'], player, win, deaths)      
            if result == "win":
                result = winScreen(win, player.name,deaths)

                play = False
                restart = False
                break
            elif result == "lose":
                deaths += 1
        elif result == "died":
            deaths += 1
        
        elif result == "continue":
            while True and restart:
                result = twoDoorRoom(rooms["twoDoor"], player, win)
                
                if result == "riddle":
                    result = goblinRiddleRoom(rooms["goblinRiddle"], player, win)

                    if result == "died":
                        deaths += 1
                        restart = False
                        break

                    elif result == "continue":
                        while True:
                            result = threeDoorRoom(rooms["threeDoor"], player, win)

                            if result == "died":
                                deaths += 1
                                restart = False
                                break

                            elif result == "chest":
                                result = hallChestRoom(rooms["hallChest"], player, win)

                                if result == "bird":
                                    result = birdRooom(rooms["bird"], player, win)
                                    if result == "continue":
                                        continue

                                    elif result == "died":
                                        deaths += 1
                                        restart = False
                                        break

                                    elif result == "finalBoss":
                                        result = finalBossRoom(rooms["finalBoss"], player, win, deaths)

                                        if result == "died":
                                            deaths += 1
                                            restart = False
                                            break
                                        
                                        elif result == "win":
                                            result = winScreen(win, player.name,deaths)

                                            play = False
                                            restart = False
                                            break
                                        
                            elif result == "big":
                                result = bigGoblinRoom(rooms["bigGoblin"], player, win)

                                if result == "3doors":
                                    continue

                                elif result == "died":
                                    deaths += 1
                                    restart = False
                                    break

                elif result == "longhallway":
                    result  = longHallwayRoom(rooms["longhallway"], player, win)

                    if result == "boss":
                        result  = finalBossRoom(rooms["finalBoss"], player, win)

                        if result == "win":
                            winScreen(win, player.name,deaths)

                          
                            play = False
                            restart = False
                            
                            break
                        elif result == "lose":
                            deaths += 1
                            restart = False
                            break

                    elif result == "2doors":
                        continue

            
       
    done()     
    sys.exit()     

    
    
    



if __name__ == "__main__":
    main()