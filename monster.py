import random

'''
    This class describes a monster
'''
class Monster(object):
    
    def __init__(self, name, attackDamage, health) -> None:
        self.name = name
        self.health = health
        self.attackDamage = attackDamage
        self.accuracy = 10
        
    '''
        Returns True if the attack hit, else returns False
    ''' 
    def attack(self) -> bool:
        if random.randint(0, 20) <= self.accuracy:
            return True
        
        return False