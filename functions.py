from turtle import *

'''
    Draws the starting health bar
'''
def drawStartHealth():
    turtle = Turtle()
    turtle.speed(0)
    turtle.hideturtle()
    turtle.penup()
    turtle.goto(-700, 350)
    turtle.pendown()
    turtle.fillcolor("green2")
    turtle.begin_fill()
    turtle.forward(300)
    turtle.right(90)
    turtle.forward(50)
    turtle.right(90)
    turtle.forward(300)
    turtle.right(90)
    turtle.forward(50)
    turtle.right(90)
    turtle.end_fill()

    
'''
    Decreases the health bar when the user is hit
'''
def decreaseHealthBar(health):
    
    if health < 0: # Makes sure that the user health bar does not go below 0
        health = 0

    healthAmt = 100 - health
    
    
    turtle = Turtle()
    turtle.speed(0)
    turtle.hideturtle()
    turtle.penup()
    turtle.goto(-400, 350)
    turtle.pendown()
    turtle.right(180)
    turtle.fillcolor("red2")
    turtle.begin_fill()
    turtle.forward(healthAmt * 3)
    turtle.left(90)
    turtle.forward(50)
    turtle.left(90)
    turtle.forward(healthAmt * 3)
    turtle.left(90)
    turtle.forward(50)
    turtle.left(90)
    turtle.end_fill()
    
'''
    Draws the narration text on the screen
'''
def writeNarration(text):
    turtle = Turtle()
    turtle.hideturtle()
    turtle.color("firebrick1")
    turtle.penup()
    turtle.goto(0,200)
    turtle.pendown()
    turtle.write(text, font=("Verdana", 20, "normal"), align="center")
    return turtle

'''
    Displays the winning screen
'''
def winScreen(win, playerName, deaths):
    win.clear()
    turtle = Turtle()
    turtle.hideturtle()
    turtle.write(f"{playerName} beat Lazarus Labyrinth with {deaths} death(s)!!", font=("Verdana", 40, "normal"), align="center")




    

    

    
    
    
