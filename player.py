import random

'''
    This class describes a player
'''
class Player(object):
    
    def __init__(self, name) -> None:
        self.name = name
        self.money = 20
        self.health = 100
        self.attackDamage = 10
        self.accuracy = 15
        self.inventory = []
        self.goblinDead = False
        self.door3 = False
        self.conditions = {
            "goblinFriend" : False,
            "helpCalled" : False,
            "understandsLife" : False,
            "ancientSword" : False
        }
        
    '''
        Returns True if the attack hit, else returns False
    '''
    def attack(self) -> bool:
        if random.randint(0, 20) <= self.accuracy: # if attack hits
            return True
        
        return False

    '''
        Resets some conditions that are changed throughout the run when the user dies
    '''
    def resetConditions(self):

        self.conditions["goblinFriend"] = False
        self.conditions["helpCalled"] = False
        self.goblinDead = False
        self.door3 = False
        self.health = 100
        self.accuracy = 15
                
                
    